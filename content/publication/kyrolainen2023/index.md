---
abstract: "When exploring the characteristics of a discourse domain associated with texts, keyword analysis is widely used in corpus linguistics. However, one of the challenges facing this method is the evaluation of the quality of the keywords. Here, we propose casting keyword analysis as a prediction problem with the goal of discriminating the texts associated with the target corpus from the reference corpus. We demonstrate that, when using linear support vector machines, this approach can be used not only to quantify the discrimination between the two corpora, but also extract keywords. To evaluate the keywords, we develop a systematic and rigorous approach anchored to the concepts of usefulness and relevance used in machine learning. The extracted keywords are compared with the recently proposed text dispersion keyness measure. We demonstrate that that our approach extracts keywords that are highly useful and linguistically relevant, capturing the characteristics of their discourse domain."
authors: 
- Aki-Juhani Kyröläinen
- Veronika Laippala
date: "2023-01-01"
doi: "https://dx.doi.org/10.3389/frai.2022.975729"
featured: false
projects: ""
publication: Predictive keywords - Using machine learning to explain document characteristics
publication_types: 
- "2"
selected: false
summary: ""
title: Predictive keywords - Using machine learning to explain document characteristics
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

---
abstract: "Input saliency methods have recently become a popular tool for explaining predictions of deep learning models in NLP. Nevertheless, there has been little work investigating methods for aggregating prediction-level explanations to the class level, nor has a framework for evaluating such class explanations been established. We explore explanations based on XLM-R and the Integrated Gradients input attribution method, and propose 1) the Stable Attribution Class Explanation method (SACX) to extract keyword lists of classes in text classification tasks, and 2) a framework for the systematic evaluation of the keyword lists. We find that explanations of individual predictions are prone to noise, but that stable explanations can be effectively identified through repeated training and explanation. We evaluate on web register data and show that the class explanations are linguistically meaningful and distinguishing of the classes."
authors: 
- Samuel Rönnqvist
- Aki-Juhani Kyröläinen
- Amanda Myntti
- Filip Ginter
- Veronika Laippala
date: "2022-01-01"
doi: "https://dx.doi.org/10.18653/v1/2022.findings-acl.85"
featured: false
projects: ""
publication: Explaining classes through stable word attributions
publication_types: 
- "1"
selected: false
summary: ""
title: Explaining classes through stable word attributions
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

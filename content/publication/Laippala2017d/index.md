---
abstract: "This article presents dependency profiles (DPs) as an empirical method to investigate linguistic elements and their application to the study of 24 discourse connectives in the 3.7-billion token Finnish Internet Parsebank (http://bionlp-www.utu.fi/dep_search/). DPs are based on co-occurrence patterns of the discourse connectives with dependency syntax relations. They follow the assumption of usage-based models, according to which the semantic and functional properties of linguistic expressions arise based on their distributional characteristics. We focus on the typical usage patterns reflected by the DPs and the (dis)similarities among discourse connectives that these patterns reveal. We demonstrate that 1) DPs can be analyzed with clustering to obtain linguistically meaningful groupings among the connectives and that 2) the clustering can be combined with support vector machines to obtain generic and stable linguistic characteristics of the discourse connectives. We show that this data-driven method offers support for previous results and reveals novel tendencies outside the scope of studies on smaller corpora. As the method is based on automatic syntactic analysis following the cross-linguistic universal dependencies, it does not require manual annotation and can be applied to a number of languages and in contrastive studies."
authors: 
- Veronika Laippala
- Aki-Juhani Kyröläinen
- Jenna Kanerva
- Filip Ginter
date: "2018-01-01"
doi: "https://dx.doi.org/10.1515/cllt-2017-0031"
featured: false
projects: ""
publication: Dependency profiles in the large-scale analysis of discourse connectives
publication_types: 
- "2"
selected: false
summary: ""
title: Dependency profiles in the large-scale analysis of discourse connectives
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

---
abstract: "Personhood is a complex concept in gerontological research. It is often used to explore the maintenance or reconstruction of self-identity. Narrative analysis has commonly explored how the stories that older individuals living with cognitive impairment(s) produce preserve the personhood that is perceived to be threatened by cognitive decline. This article moves beyond this exploration by focusing on the experiences of aging more generally to better understand how non-cognitively impaired older adults construct personhood through narrative writing. This article uses thematic narrative analysis and argues that older adults articulate personhood through written narratives by creating coherent constructions of self. Importantly, it demonstrates that older adults are concerned with maintaining a sense of self rather than embracing changed aspects of identity in accordance with popularized conceptualizations of personhood as well as ideals of coherence and consistency promoted in successful and active aging discourses."
authors: 
- Kaitlyn Jaggers
- James Gillet
- Victor Kuperman
- Aki-Juhani Kyröläinen
- Ranil Sonnadara
date: "2022-01-01"
doi: "https://dx.doi.org/10.1016/j.jaging.2022.101040"
featured: false
projects: ""
publication: Personhood and aging - Exploring the written narratives of older adults as articulations of personhood in later life
publication_types: 
- "2"
selected: false
summary: ""
title: Personhood and aging - Exploring the written narratives of older adults as articulations of personhood in later life
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

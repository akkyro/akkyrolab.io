---
abstract: "What makes a literate person? What leads to literacy gains and losses within and between individuals and countries? This paper provides new evidence that helps answer these questions. The present comparative analysis of literacy is based on large representative samples from the Survey of Adult Skills conducted in 33 countries, with 25--65 year old participants. We provide, for the first time, estimates of relative importance for a comprehensive set of experiential factors, motivations, incentives, parental influence, demands of workplace, and other predictors of influence. We sketch a configuration of factors that predicts an 'ideal' reader, i.e., the optimal literacy performance. Moreover, we discover a pivotal role of the age effect in predicting variability between countries. Countries with the highest literacy scores are the ones where literacy decreases with age the most strongly. We discuss this finding against current accounts of aging effects, cohort effects and others. Finally, we provide methodological recommendations for experimental studies of aging in cognitive tasks like reading."
authors: 
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: "2021-01-01"
doi: "https://dx.doi.org/10.1371/journal.pone.0243763"
featured: false
projects: ""
publication: Predictors of literacy in adulthood - Evidence from 33 countries
publication_types: 
- "2"
selected: false
summary: ""
title: Predictors of literacy in adulthood - Evidence from 33 countries
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

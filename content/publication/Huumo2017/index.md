---
abstract: "In Standard Finnish, the case marking of the S argument in existential clauses alternates between the nominative and the partitive, while A arguments of transitive clauses are always in the nominative. In actual usage, however, the partitive is used occasionally to mark some A arguments as well (the partitive A). We extracted data from the Finnish Internet Parsebank, an ongoing project that crawls the Finnish Internet and gathers massive corpora with an automatic morphological and syntactic analysis. We focus on the properties of the argument structure of the partitive A construction, and present a qualitative analysis for the properties of the A and O arguments and their contribution to the semantics of this particular structure. The role of the verb is then analyzed, with a focus on frequency of use and semantic similarity. We utilize semantic vector models in constructing similarity-based relations among the verbs, and present an analysis of semantic similarity based on the word2vec model. This measure of similarity is based on estimates of co-occurrence of words in the corpus, but it can scale efficiently up to billions of estimates. Overall, the analysis presented shows the niche occupied by the partitive A in the Finnish lexicon and the contribution of individual arguments, frequency of use, and semantic similarity."
authors: 
- Tuomas Huumo
- Aki-Juhani Kyröläinen
- Jenna Kanerva
- M. Juhani Luotolahti
- Tapio Salakoski
- Filip Ginter
- Veronika Laippala
date: "2017-01-01"
featured: false
projects: ""
publication: Distributional semantics of the partitive A argument construction in Finnish
publication_types: 
- "6"
selected: false
summary: ""
title: Distributional semantics of the partitive A argument construction in Finnish
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

---
abstract: "This cross-linguistic study investigated the impact of spelling errors on reading behavior in five languages (Chinese, English, Finnish, Greek, and Hebrew). Learning theories predict that correct and incorrect spelling alternatives (e.g., 'tomorrow' and 'tommorrow') provide competing cues to the sound and meaning of a word: The closer the alternatives are to each other in their frequency of occurrence, the more uncertain the reader is regarding the spelling of that word. An information-theoretic measure of entropy was used as an index of uncertainty. Based on theories of learning, we predicted that higher entropy would lead to slower recognition of words even when they are spelled correctly. This prediction was confirmed in eye-tracking sentence-reading experiments in five languages widely variable in their writing systems' phonology and morphology. Moreover, in each language, we observed a characteristic Entropy texttimes Frequency interaction; arguably, its functional shape varied as a function of the orthographic transparency of a given written language."
authors: 
- Victor Kuperman
- Amalia Bar-On
- Raymond Bertram
- Rober Boshra
- Avital Deutsch
- Aki-Juhani Kyröläinen
- Barbara Mathiopoulou
- Gaisha Oralova
- Athanassios Protopapas
date: "2021-01-01"
doi: "https://dx.doi.org/10.1037/xge0001038"
featured: false
projects: ""
publication: Spelling errors affect reading behavior across languages
publication_types: 
- "2"
selected: false
summary: ""
title: Spelling errors affect reading behavior across languages
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

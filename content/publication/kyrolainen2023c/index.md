---
abstract: "The reported psychological impact of the COVID-19 pandemic and related public health measures included a decline in cognitive functioning in older adults. Cognitive functioning is known to correlate with the lexical and syntactic complexity of an individual's linguistic productions. We examined written narratives from the CoSoWELL corpus (v 1.0), collected from over 1,000 U.S. and Canadian older adults (55+ y.o.) before and during the first year of the pandemic. We expected a decrease in the linguistic complexity of the narratives, given the oft-reported reduction in cognitive functioning associated with COVID-19. Contrary to this expectation, all measures of linguistic complexity showed a steady increase from the pre-pandemic level throughout the first year of the global lockdown. We discuss possible reasons for this boost in light of exiting theories of cognition and offer a speculative link between the finding and reports of increased creativity during the pandemic."
authors: 
- Megan Karabin
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: "NA"
featured: false
projects: ""
publication: Increase in linguistic complexity and creativity in older adults during COVID-19
publication_types: 
- "2"
selected: false
summary: ""
title: Increase in linguistic complexity and creativity in older adults during COVID-19
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

---
abstract: "Määrällinen korpuslingvistiikka on kielenkäytön, sen vaihtelun ja kielen rakenteen tutkimusta laajojen aineistojen pohjalta. Analyysi on empiiristä, ja se perustuu laskennallisiin, tietokoneella toteutettuihin menetelmiin. Aineistot ovat tyypillisesti konelukuisia, ja ne on koottu tiettyjen kriteerien mukaan (ks. Korpusaineistot tässä kirjassa)."
authors: 
- Aki-Juhani Kyröläinen
- Veronika Laippala
date: "2020-01-01"
doi: "https://dx.doi.org/10.21435/skst.1457"
featured: false
projects: ""
publication: Määrällinen korpuslingvistiikka (Quantitative corpus linguistics)
publication_types: 
- "6"
selected: false
summary: ""
title: Määrällinen korpuslingvistiikka (Quantitative corpus linguistics)
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

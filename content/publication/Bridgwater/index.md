---
abstract: "Language processing is incremental and inherently predictive. Against this theoretic backdrop, we investigated the role of upcoming structural information in the comprehension of the English dative alternation. The use of eye-tracking enabled us to examine both the time course and locus of the effect associated with (a) structural expectations based on a lifetime of experience with language, and (b) rapid adaptation of the reader to the local statistics of the experiment. We quantified (a) as a verb subcategorization bias toward dative alternatives, and (b) as distributional biases in the syntactic input during the experiment. A reliable facilitatory effect of the verb bias was only observed in the double-object datives and only in the disambiguation region of the second object. Furthermore, structural priming led to an earlier locus of the verb bias effect, suggesting an interaction between (a) and (b). Our results offer a new outlook on the utilization of syntactic expectations during reading, in conjunction with rapid adaptation to the immediate linguistic environment. We demonstrate that this utilization is both malleable and strategic."
authors: 
- Emma Bridgwater
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: "2019-01-01"
doi: "https://dx.doi.org/10.1037/cep0000173"
featured: false
projects: ""
publication: The influence of syntactic expectations on reading comprehension is malleable and strategic - An eye-tracking study of English dative alternation
publication_types: 
- "2"
selected: false
summary: ""
title: The influence of syntactic expectations on reading comprehension is malleable and strategic - An eye-tracking study of English dative alternation
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

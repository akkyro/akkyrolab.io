---
abstract: "Emotions play a fundamental role in language learning, use and processing. Words denoting positivity account for a larger part of the lexicon than words denoting negativity, and they also tend to be used more frequently, a phenomenon known as positivity bias. However, language experience changes over an individualrqs lifetime making the examination of the emotion-laden lexicon an important topic not only across the life span but also across languages. Furthermore, existing theories predict a range of different age-related trajectories in processing valenced words. The present study pits all of these predictions against written productions (Facebook status updates from over 20,000 users) and behavioral data from three publicly available mega-studies on different languages, namely English, Dutch, and Spanish across adulthood. The production data demonstrated an increase in positive word types and tokens with advancing age. In terms of comprehension, the results showed a uniform and consistent effect of valence across languages and cohorts based on data from a visual word recognition task. The difference in RTs to very positive and very negative words declined with age, with responses to positive words slowing down more strongly with age than responses to negative words. We argue that the results stem from lifelong learning and emotion regulation: advancing age is accompanied by an increased type frequency of positive words in language production, which is mirrored as a discrimination penalty in comprehension. To our knowledge, this is the first study to simultaneously target both language production and comprehension across adulthood and in a cross-linguistic perspective."
authors: 
- Aki-Juhani Kyröläinen
- Emmanuel Keuleers
- Paweł Mandera
- Marc Brysbaert
- Victor Kuperman
date: "2021-01-01"
doi: "https://dx.doi.org/10.1037/xge0000950"
featured: false
projects: ""
publication: Affect across adulthood - Evidence from English, Dutch and Spanish
publication_types: 
- "2"
selected: false
summary: ""
title: Affect across adulthood - Evidence from English, Dutch and Spanish
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

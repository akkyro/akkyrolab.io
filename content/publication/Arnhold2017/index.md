---
abstract: "Focus marking is an important function of prosody in many languages. While many phonological accounts concentrate on fundamental frequency (F0), studies have established several additional cues to information structure. However, the relationship between these cues is rarely investigated. We simultaneously analyzed five prosodic cues to focus---F0 range, word duration, intensity, voice quality, the location of the F0 maximum, and the occurrence of pauses---in a set of 947 simple Subject Verb Object (SVO) sentences uttered by 17 native speakers of Finnish. Using random forest and generalized additive mixed modelling, we investigated the systematicity of prosodic focus marking, the importance of each cue as a predictor, and their functional shape. Results indicated a highly consistent differentiation between narrow focus and givenness, marked by at least F0 range, word duration, intensity, and the location of the F0 maximum, with F0 range being the most important predictor. No cue had a linear relationship with focus condition. To account for the simultaneous significance of several predictors, we argue that these findings support treating multiple prosodic cues to focus in Finnish as correlates of prosodic phrasing. Thus, we suggest that prosodic phrasing, having multiple functions, is also marked with multiple cues to enhance communicative efficiency."
authors: 
- Anja Arnhold
- Aki-Juhani Kyröläinen
date: "2017-01-01"
doi: "https://dx.doi.org/10.5334/labphon.78"
featured: false
projects: ""
publication: Modelling the interplay of multiple cues in prosodic focus marking
publication_types: 
- "2"
selected: false
summary: ""
title: Modelling the interplay of multiple cues in prosodic focus marking
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

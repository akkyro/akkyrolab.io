---
abstract: "NA"
authors: 
- Veronika Laippala
- Jesse Egbert
- Douglas Biber
- Aki-Juhani Kyröläinen
date: "2021-01-01"
doi: "https://dx.doi.org/10.1007/s10579-020-09519-z"
featured: false
projects: ""
publication: Exploring the role of lexis and grammar for the stable identification of register in an unrestricted corpus of web documents
publication_types: 
- "2"
selected: false
summary: ""
title: Exploring the role of lexis and grammar for the stable identification of register in an unrestricted corpus of web documents
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

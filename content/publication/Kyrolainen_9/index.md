---
abstract: "This paper presents the Cognitive and Social WELL-being (CoSoWELL) project that consists of two components. One is a large corpus of narratives written by over 1000 North American older adults (55+ years old) in five test sessions before and during the first year of the COVID-19 pandemic. The other component is a rich collection of socio-demographic data collected through a survey from the same participants. This paper introduces the first release of the corpus consisting of 1.3 million tokens and the survey data (CoSoWELL version 1.0). It also presents a series of analyses validating design decisions for creating the corpus of narratives written about personal life events that took place in the distant past, recent past (yesterday) and future, along with control narratives. We report results of computational topic modeling and linguistic analyses of the narratives in the corpus, which track the time-locked impact of the COVID-19 pandemic on the content of autobiographical memories before and during the COVID-19 pandemic. The main findings demonstrate a high validity of our analytical approach to unique narrative data and point to both the locus of topical shifts (narratives about recent past and future) and their detailed timeline. We make the CoSoWELL corpus and survey data available to researchers and discuss implications of our findings in the framework of research on aging and autobiographical memories under stress."
authors: 
- Aki-Juhani Kyröläinen
- James Gillet
- Megan Karabin
- Gary Libben
- Ranil Sonnadara
- Victor Kuperman
date: "2022-01-01"
doi: "https://dx.doi.org/10.3758/s13428-022-01926-0"
featured: false
projects: ""
publication: Cognitive and social well-being in older adulthood - The CoSoWELL corpus of written life stories
publication_types: 
- "2"
selected: false
summary: ""
title: Cognitive and social well-being in older adulthood - The CoSoWELL corpus of written life stories
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

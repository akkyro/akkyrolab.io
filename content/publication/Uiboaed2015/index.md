---
abstract: "Data visualization is an integral part of scientific inquiry in order to represent data and communicate findings. Recent developments such as the rise of large-scale corpora show that techniques to relate linguistically informed analysis and spatial data visualization have become increasingly important for quantitative analysis. Although spatial data visualization has gained momentum, these techniques may not be readily available for small or understudied languages. Here, we give an introduction to spatial data visualization using publicly available resources. We use case studies on Estonian and Votic data to illustrate certain basic tasks in quantitative dialectology. We give solutions to create spatial maps based on either self-extracted coordinates or Google Maps. These maps can be used as a base layer and additional information, such as metadata and frequency distributions, can be represented on top of them. This approach can also be applied to statistical analysis. We illustrate this by carrying out a cluster analysis and its visualization using Google Maps. Thus, a toolkit is provided for quantitative analysis and spatial visualization in dialectology."
authors: 
- Kristel Uiboaed
- Aki-Juhani Kyröläinen
date: "2015-01-01"
doi: "https://dx.doi.org/10.5128/ERYa11.17"
featured: false
projects: ""
publication: Keeleteaduslike andmete ruumilisi visualiseerimisv~oimalusi (Techniques for spatial data visualization in linguistics)
publication_types: 
- "2"
selected: false
summary: ""
title: Keeleteaduslike andmete ruumilisi visualiseerimisv~oimalusi (Techniques for spatial data visualization in linguistics)
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

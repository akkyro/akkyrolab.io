---
abstract: "NA"
authors: 
- Veronika Laippala
- Aki-Juhani Kyröläinen
- Johanna Komppa
- Maria Vilkuna
- Jyrki Kalliokoski
- Filip Ginter
date: "2016-01-01"
featured: false
projects: ""
publication: Sentence-initial discourse relational devices in the Finnish Internet
publication_types: 
- "1"
selected: false
summary: ""
title: Sentence-initial discourse relational devices in the Finnish Internet
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

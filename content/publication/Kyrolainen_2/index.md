---
abstract: "Intuitively, some predicates have a better fit with certain arguments than others. Usage-based models of language emphasize the importance of semantic similarity in shaping the structuring of constructions (form and meaning). In this study, we focus on modeling the semantics of transitive constructions in Finnish and present an autoencoder-based neural network model trained on semantic vectors based on Word2vec. This model builds on the distributional hypothesis according to which semantic information is primarily shaped by contextual information. Specifically, we focus on the realization of the object. The performance of the model is evaluated in two tasks: a pseudo-disambiguation and a cloze task. Additionally, we contrast the performance of the autoencoder with a previously implemented neural model. In general, the results show that our model achieves an excellent performance on these tasks in comparison to the other models. The results are discussed in terms of usage-based construction grammar."
authors: 
- Aki-Juhani Kyröläinen
- M. Juhani Luotolahti
- Filip Ginter
date: "2017-01-01"
doi: "https://dx.doi.org/10.12697/jeful.2017.8.2.04"
featured: false
projects: ""
publication: An autoencoder-based neural network model for selectional preference - Evidence from pseudodisambiguation and cloze tasks
publication_types: 
- "2"
selected: false
summary: ""
title: An autoencoder-based neural network model for selectional preference - Evidence from pseudodisambiguation and cloze tasks
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

---
abstract: "Psychological research, including research into adult reading, is frequently based on convenience samples of undergraduate students. This practice raises concerns about the external validity of many accepted findings. The present study seeks to determine how strong this student sampling bias is in literacy and numeracy research. We use the nationally representative cross-national data from the Programme for the International Assessment of Adult Competencies to quantify skill differences between (i) students and the general population aged 16--65, and (ii) students and age-matched non-students aged 16--25. The median effect size for the comparison (i) of literacy scores across 32 countries was d = .56, and for comparison (ii) d = .55, which exceeds the average effect size in psychological experiments (d = .40). Numeracy comparisons (i) and (ii) showed similarly strong differences. The observed differences indicate that undergraduate students are not representative of the general population nor age-matched non-students."
authors: 
- Heather Wild
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: "2022-01-01"
doi: "https://dx.doi.org/10.1371/journal.pone.0271191"
featured: false
projects: ""
publication: How representative are student convenience samples? A study of literacy and numeracy skills in 32 countries
publication_types: 
- "2"
selected: false
summary: ""
title: How representative are student convenience samples? A study of literacy and numeracy skills in 32 countries
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

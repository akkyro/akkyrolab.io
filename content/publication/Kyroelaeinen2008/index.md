---
abstract: "The relationship between frequency, to be more precise, low-frequency 1and salience is explored in this paper . I will build my argument step by step: the concept of salience is divided into two concepts: cognitive and linguistic salience; the premise concerns the latter in this paper. Further, it is argued that at least four properties play a decisive role in determining linguistic salience: productivity, idiosyncrasy, complexity of form, and complexity of meaning. The empirical body of data consists of one Russian impersonal construction type, the dative impersonal construction. In the first section, the concept of salience is elaborated more precisely. The second section presents the Russian impersonal construction type under investigation. In the third section, the data are discussed in detail. The following sections are thereafter used to analyze the corpus-based data, and to discuss the findings in relation to the concept of linguistic salience. In addition, the dative impersonal construction type has not been studied with a corpus linguistic approach before, at least to my knowledge. The data are fully based on corpus material, thus allowing the evaluation of claims made in previous studies on this particular construction type."
authors: 
- Aki-Juhani Kyröläinen
date: "2008-01-01"
featured: false
projects: ""
publication: Low-frequency constructions and salience - A case study on Russian verbs of motion of dative impersonal construction type
publication_types: 
- "6"
selected: false
summary: ""
title: Low-frequency constructions and salience - A case study on Russian verbs of motion of dative impersonal construction type
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

---
abstract: "NA"
authors: 
- Daniil Gnetov
- Aki-Juhani Kyröläinen
date: "2019-01-01"
featured: false
projects: ""
publication: Quantifications of morphological family size - Evidence from visual lexical decision task in Russian
publication_types: 
- "1"
selected: false
summary: ""
title: Quantifications of morphological family size - Evidence from visual lexical decision task in Russian
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

---
abstract: "This article examines the automatic identification of Web registers, that is, text varieties such as news articles and reviews. Most studies have focused on corpora restricted to include only preselected classes with well-defined characteristics. These corpora feature only a subset of documents found on the unrestricted open Web, for which register identification has been particularly difficult because the range of linguistic variation on the Web is known to be substantial. As part of this study, we present the first open release of the Corpus of Online Registers of English (CORE), which is drawn from the unrestricted open Web and, currently, is the largest collection of manually annotated Web registers. Furthermore, we demonstrate that the CORE registers can be automatically identified with competitive results, with the best performance being an F1-score of 68% with the deep learning model BERT. The best performance was achieved using two modeling strategies. The first one involved modeling the registers using propagated register labels, that is, repeating the main register label along with its corresponding subregister label in a multilabel model. In the second one, we explored how the length of the document affects model performance, discovering that the beginning provided superior classification accuracy. Overall, the current study presents a systematic approach for the automatic identification of a large number of Web registers from the unrestricted Web, hence providing new pathways for future studies."
authors: 
- Veronika Laippala
- Samuel Rönnqvist
- Miika Oinonen
- Aki-Juhani Kyröläinen
- Anna Salmela
- Douglas Biber
- Jesse Egbert
- Sampo Pyysalo
date: "2022-01-01"
doi: "https://dx.doi.org/10.1007/s10579-022-09624-1"
featured: false
projects: ""
publication: Register identification from the unrestricted open Web using the Corpus of Online Registers of English
publication_types: 
- "2"
selected: false
summary: ""
title: Register identification from the unrestricted open Web using the Corpus of Online Registers of English
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

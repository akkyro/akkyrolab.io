---
abstract: "NA"
authors: 
- Aki-Juhani Kyröläinen
- Kristina Geeraert
date: "2013-01-01"
featured: false
projects: ""
publication: The Relationship between form and meaning - Modelling semantic densities of English monomorphemic verbs
publication_types: 
- "1"
selected: false
summary: ""
title: The Relationship between form and meaning - Modelling semantic densities of English monomorphemic verbs
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

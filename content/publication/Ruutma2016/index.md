---
abstract: "NA"
authors: 
- Mirjam Ruutma
- Aki-Juhani Kyröläinen
- Marja-Liisa Pilvik
- Kristel Uiboaed
date: "2016-01-01"
featured: false
projects: ""
publication: Ambipositsioonide morfosüntaktilise varieerumise kirjeldusi kvantitatiivsete profiilide abil (Descriptions of the morphosyntactic variation of ambipositions by means of quantitative profiles)
publication_types: 
- "2"
selected: false
summary: ""
title: Ambipositsioonide morfosüntaktilise varieerumise kirjeldusi kvantitatiivsete profiilide abil (Descriptions of the morphosyntactic variation of ambipositions by means of quantitative profiles)
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

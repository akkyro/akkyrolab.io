---
abstract: "Highlights: The analysis was based on a large-scale corpus of tweets in English in the Twitter discussion with the hashtag #jesuisCharlie. The analysis was conducted sequentially with mixed methods: language technology method, quantitative analysis of modality, and digital discourse analysis of the opinions expressed in the tweets. The data contained multiple topics that were organized into five clusters. One of the clusters was more modalized than others. All of the clusters contained expressions of affectivity and solidarity, but they also contained diversified and polarized opinions."
authors: 
- Marjut Johansson
- Aki-Juhani Kyröläinen
- Filip Ginter
- Lotta Lehti
- Attila Krizsán
- Veronika Laippala
date: "2018-01-01"
doi: "https://dx.doi.org/10.1016/j.pragma.2018.03.007"
featured: false
projects: ""
publication: Opening up #jesuisCharlie anatomy of a Twitter discussion with mixed methods
publication_types: 
- "2"
selected: false
summary: ""
title: Opening up #jesuisCharlie anatomy of a Twitter discussion with mixed methods
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

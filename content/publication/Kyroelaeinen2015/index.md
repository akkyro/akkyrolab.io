---
abstract: "The status of oblique subjects is a contested topic. Subjecthood is typically defined relative to syntactic tests in formal approaches and different tests have been proposed in the literature. However, they have not been applied systematically to cover a wide range of constructions such as oblique subjects in Russian. This study presents a clustered model of subjecthood building on ten construction types and twenty features of subjecthood in Russian based on construction grammar and Keenanrqs (1976) study. The structure of the model indicates that Russian has two clustered subject constructions: the nominative and the dative. These types motivate the deviations displayed by the other constructions. Hence, subjecthood is defined as a fully motivated network structure."
authors: 
- Aki-Juhani Kyröläinen
date: "2015-01-01"
featured: false
projects: ""
publication: From canon and monolith to clusters - A constructionist model of subjecthood in Russian
publication_types: 
- "6"
selected: false
summary: ""
title: From canon and monolith to clusters - A constructionist model of subjecthood in Russian
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

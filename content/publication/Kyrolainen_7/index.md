---
abstract: "The topic of affective development over the lifespan is at the forefront of psychological science. One of the intriguing findings in this area is superior emotion regulation and increased positivity in older rather than younger adults. This paper aims to contribute to the empirical base of studies on the role of affect in cognition. We report a new dataset of valence (positivity) ratings to 3,600 English words collected from North American and British English-speaking younger (below 65 years of age) and older adults (65 years of age and older) during the COVID-19 pandemic. This dataset represents a broad range of valence and a rich selection of semantic categories. Our analyses of the new data pitted against comparable pre-pandemic (2013) data from younger counterparts reveal differences in the overall distribution of valence related both to age and the psychological fallout of the pandemic. Thus, we found at the group level that older participants produced higher valence ratings overall than their younger counterparts before and especially during the pandemic. Moreover, valence ratings saw a super-linear increase after the age of 65. Together, these findings provide new evidence for emotion regulation throughout adulthood, including a novel demonstration of greater emotional resilience in older adults to the stressors of the pandemic."
authors: 
- Aki-Juhani Kyröläinen
- Javon Luke
- Gary Libben
- Victor Kuperman
date: "2021-01-01"
doi: "https://dx.doi.org/10.3758/s13428-021-01740-0"
featured: false
projects: ""
publication: Valence norms for 3,600 English words collected during the COVID-19 pandemic - Effects of age and the pandemic
publication_types: 
- "2"
selected: false
summary: ""
title: Valence norms for 3,600 English words collected during the COVID-19 pandemic - Effects of age and the pandemic
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

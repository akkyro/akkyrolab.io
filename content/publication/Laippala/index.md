---
abstract: "This study presents a methodological toolbox for big data analysis of linguistic constructions by introducing dependency profiles, i.e., co-occurrences of linguistic elements with syntax information. These were operationalized by reconstructing sentences as delexicalized syntactic biarcs, subtrees of dependency analyses. As a case study, we utilize these dependency profiles to explore usage patterns associated with emoticons, the graphic representations of facial expressions. These are said to be characteristic of Computer-Mediated Communication, but typically studied only in restricted corpora. To analyze the 3.7-billion token Finnish Internet Parsebank we use as data, we apply clustering and support vector machines. The results show that emoticons are associated with three typical usage patterns: stream of the writerrqs consciousness, narrative constructions and elements guiding the interaction and expressing the writerrqs reactions by means of interjections and discourse particles. Additionally, the more frequent emoticons, such as:), are used differently than the less frequent ones, such as ^_^."
authors: 
- Veronika Laippala
- Aki-Juhani Kyröläinen
- Jenna Kanerva
- M. Juhani Luotolahti
- Tapio Salakoski
- Filip Ginter
date: "2017-01-01"
doi: "https://dx.doi.org/10.12697/jeful.2017.8.2.05"
featured: false
projects: ""
publication: Dependency profiles as a tool for big data analysis of linguistic constructions - A case study of emoticons
publication_types: 
- "2"
selected: false
summary: ""
title: Dependency profiles as a tool for big data analysis of linguistic constructions - A case study of emoticons
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

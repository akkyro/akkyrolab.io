---
abstract: "This paper brings new insight to poverty and social exclusion through an analysis of how poverty-related issues are commented on in the largest online discussion forum in Finland: Suomi24 (lqFinland24rq). For data, we use 32,407 posts published in the forum in 2014 that contain the word köyhä (lqpoorrq) or a predefined semantically similar word. We apply the Corpus-Assisted Discourse Studies (CADS) method, which combines quantitative methods and qualitative discourse analysis. This methodological solution allows us to analyse both large-scale tendencies and detailed expressions and nuances on how poverty is discussed. The quantitative analysis is conducted with topic modelling, an unsupervised machine learning method used to examine large volumes of unlabelled text. Our results show that discussions concerning poverty are multifaceted and can be broken down into several categories, including politics; money, income and spending; and unequal access to goods. This suggests that poverty affects the lives of people with low income in a comprehensive way. Furthermore, it is shown that the posts include self-expression that displays both the juxtaposition of social groups, e.g., between the rich and poor, and between politicians and citizens, as well as peer support and giving advice."
authors: 
- Lotta Lehti
- Milla Luodonpää-Manni
- Jarmo H. Jantunen
- Aki-Juhani Kyröläinen
- Aleksi Vesanto
- Veronika Laippala
date: "2020-01-01"
featured: false
projects: ""
publication: Commenting on poverty online - A corpus-assisted discourse study of Suomi24 forum
publication_types: 
- "2"
selected: false
summary: ""
title: Commenting on poverty online - A corpus-assisted discourse study of Suomi24 forum
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

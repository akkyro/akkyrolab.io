---
date: "2021-10-31"
external_link: 
image:
  caption: 
  focal_point: 
summary: University-level courses
tags:
- teaching
title: University Teaching
---

At the University of Tartu, I designed and taught four university-level courses including, *Semantics and Semantic Relations*, *Introduction to Statistical Analysis of Linguistic Data using R*, *Introduction to Data Visualization using R*, and *Introduction to R in Linguistic Applications*.

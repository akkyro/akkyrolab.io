---
date: "2021-10-31"
external_link: ""
image:
  caption: 
  focal_point: 
links:
slides: 
summary: Invited workshops
tags:
- teaching
title: Workshops
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

Recently, I designed a new workshop on using NLP methods, such as topic modeling, for analyzing data in experimental psychology.
In addition, I have also designed and taught several international workshops that have focused on the analysis of experimental data such as reaction times and gaze data using linear mixed-effects regression and generalized additive mixed-effects regression. These workshops were primarily offered as part of STEP (Spring Training in Experimental Psycholinguistics - <a href="http://ccp.artsrn.ualberta.ca/stepccp/" target="_blank">STEP@CCP</a>) which offers courses in experimental psycholinguistics at the University of Alberta (Canada).

I have also delivered several workshops focusing on experimental design and eye-tracking methods during reading at Tomsk State University (Russia) as part of the Experimental Studies of Language and Speech series.

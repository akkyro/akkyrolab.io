---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2009-10-15T13:00:00
date_end: 2009-10-15T13:00:00
draft: false
event: "Slavic Cognitive Linguistics Association (SCLA)"
event_url: ""
featured: false
location: "Prague"
projects:  ""
publishDate: 2009-10-15
selected: false
summary: ""
title: Subject, subjecthood and subjectivity
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

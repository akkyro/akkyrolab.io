---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2012-08-29T13:00:00
date_end: 2012-08-29T13:00:00
draft: false
event: "The 45th Annual Meeting of the Societas Linguistica Europaea"
event_url: ""
featured: false
location: "Stockholm"
projects:  ""
publishDate: 2012-08-29
selected: false
summary: ""
title: Multiple valency frames - Towards a density-based network model
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

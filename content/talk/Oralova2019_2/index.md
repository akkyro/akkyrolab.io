---
abstract: ""
authors: 
- Gaisha Oralova
- Rober Boshra
- Daniel Schmidtke
- Aki-Juhani Kyröläinen
- John F. Connolly
- Victor Kuperman
date: 2019-11-14T13:00:00
date_end: 2019-11-14T13:00:00
draft: false
event: "The 60th Annual Meeting of the Psychonomic Society"
event_url: ""
featured: false
location: "Montreal"
projects:  ""
publishDate: 2019-11-14
selected: false
summary: ""
title: The time course of lexical and semantic effects of complex word recognition - A combined EEG & eye-tracking study
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

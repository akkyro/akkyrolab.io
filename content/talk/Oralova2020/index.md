---
abstract: ""
authors: 
- Gaisha Oralova
- Rober Boshra
- Daniel Schmidtke
- Aki-Juhani Kyröläinen
- John F. Connolly
- Victor Kuperman
date: 2020-10-16T13:00:00
date_end: 2020-10-16T13:00:00
draft: false
event: "Words in the World (WoW) International Conference"
event_url: ""
featured: false
location: "Virtual Conference"
projects:  ""
publishDate: 2020-10-16
selected: false
summary: ""
title: The chicken or the egg? The timeline for lexical and semantic effects in derived word recognition using simultaneous recording of EEG and eye-tracking
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Victor Kuperman
- Aki-Juhani Kyröläinen
- James Gillet
- Ranil Sonnadara
date: 2020-12-07T13:00:00
date_end: 2020-12-07T13:00:00
draft: false
event: "MIRA & Labarge Research Showcase"
event_url: ""
featured: false
location: "Hamilton"
projects:  ""
publishDate: 2020-12-07
selected: false
summary: ""
title: Optimism in time of the pandemic - Evidence from story-writing
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

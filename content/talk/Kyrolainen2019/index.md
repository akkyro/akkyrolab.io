---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Filip Ginter
- Raymond Bertram
- Victor Kuperman
date: 2019-07-17T13:00:00
date_end: 2019-07-17T13:00:00
draft: false
event: "The 26th Annual Society for the Scientific Study of Reading Meeting"
event_url: ""
featured: false
location: "Toronto"
projects:  ""
publishDate: 2019-07-17
selected: false
summary: ""
title: Orthographic representations and orthographic competition - The role of spelling errors in language processing
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

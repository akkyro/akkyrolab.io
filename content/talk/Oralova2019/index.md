---
abstract: ""
authors: 
- Gaisha Oralova
- Rober Boshra
- Aki-Juhani Kyröläinen
- John Connolly
- Victor Kuperman
date: 2019-07-17T13:00:00
date_end: 2019-07-17T13:00:00
draft: false
event: "The 26th Annual Society for the Scientific Study of Reading Meeting"
event_url: ""
featured: false
location: "Toronto"
projects:  ""
publishDate: 2019-07-17
selected: false
summary: ""
title: What an incorrect use of a character tells us about the organization of Chinese mental lexicon - A combined EEG & eye-tracking study
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2011-09-08T13:00:00
date_end: 2011-09-08T13:00:00
draft: false
event: "The 44th Annual Meeting of the Societas Linguistica Europaea"
event_url: ""
featured: false
location: "Logro~no"
projects:  ""
publishDate: 2011-09-08
selected: false
summary: ""
title: Degrees and measurement of subjectivity - A logistic mixed-effects model for the Russian subjective experiencer construction
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

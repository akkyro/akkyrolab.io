---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: 2018-02-26T13:00:00
date_end: 2018-02-26T13:00:00
draft: false
event: "Night Whites 2018. The 4th St. Petersburg Winter Workshop on Experimental Studies of Speech and Language"
event_url: ""
featured: false
location: "St. Petersburg"
projects:  ""
publishDate: 2018-02-26
selected: false
summary: ""
title: It's all about the base - Age effects on morphological processing
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

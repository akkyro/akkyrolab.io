---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2008-05-29T13:00:00
date_end: 2008-05-29T13:00:00
draft: false
event: "Cognitive and Functional Perspectives on Dynamic Tendencies in Languages"
event_url: ""
featured: false
location: "University of Tartu"
projects:  ""
publishDate: 2008-05-29
selected: false
summary: ""
title: Reflexive space - A layered model for the Russian reflexive marker
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

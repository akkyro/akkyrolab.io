---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2007-04-22T13:00:00
date_end: 2007-04-22T13:00:00
draft: false
event: "Cognitive Linguistics Seminar"
event_url: ""
featured: false
location: "University of Tartu"
projects:  ""
publishDate: 2007-04-22
selected: false
summary: ""
title: Remarks on Russian reflexive verbs - Schemacity and prototype
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2011-03-29T13:00:00
date_end: 2011-03-29T13:00:00
draft: false
event: "NA"
event_url: ""
featured: false
location: "The Russian Academy of Sciences"
projects:  ""
publishDate: 2011-03-29
selected: false
summary: ""
title: Frequencies, distributions and statistical models
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

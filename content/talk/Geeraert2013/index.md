---
abstract: ""
authors: 
- Kristina Geeraert
- Aki-Juhani Kyröläinen
date: 2013-06-23T13:00:00
date_end: 2013-06-23T13:00:00
draft: false
event: "The 12th International Cognitive Linguistics Conference"
event_url: ""
featured: false
location: "University of Alberta"
projects:  ""
publishDate: 2013-06-23
selected: false
summary: ""
title: Paradigmatic levelling in English - The influence of phonological neighbours
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Vincent Porretta
- Aki-Juhani Kyröläinen
date: 2017-09-07T13:00:00
date_end: 2017-09-07T13:00:00
draft: false
event: "Architectures and Mechanisms of Language Processing (AMLaP)"
event_url: ""
featured: false
location: "Lancaster University"
projects:  ""
publishDate: 2017-09-07
selected: false
summary: ""
title: Expanding Competition Space - The Influence of Foreign Accentedness on Lexical Competition
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2016-01-11T13:00:00
date_end: 2016-01-11T13:00:00
draft: false
event: "Utuling seminar"
event_url: ""
featured: false
location: "University of Turku"
projects:  ""
publishDate: 2016-01-11
selected: false
summary: ""
title: The role of expectations in processing event structures
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2020-12-07T13:00:00
date_end: 2020-12-07T13:00:00
draft: false
event: "Experimental Studies of Language and Speech"
event_url: ""
featured: false
location: "Tomsk State University"
projects:  ""
publishDate: 2020-12-07
selected: false
summary: ""
title: Affect and adulthood
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

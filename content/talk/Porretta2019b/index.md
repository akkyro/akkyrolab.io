---
abstract: ""
authors: 
- Vincent Porretta
- Aki-Juhani Kyröläinen
- Ghadir Nassereddine
- Lori Buchanan
date: 2019-11-14T13:00:00
date_end: 2019-11-14T13:00:00
draft: false
event: "The 60th Annual Meeting of the Psychonomic Society"
event_url: ""
featured: false
location: "Montreal"
projects:  ""
publishDate: 2019-11-14
selected: false
summary: ""
title: When 'bug' creeps into memory - False memories and foreign-accented speech
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

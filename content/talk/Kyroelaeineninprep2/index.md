---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- M. Juhani Luotolahti
date: 2016-01-28T13:00:00
date_end: 2016-01-28T13:00:00
draft: false
event: "IV soome-eesti grammatikakonverents (The 4th Finnish-Estonian Grammar Conference)"
event_url: ""
featured: false
location: "University of Tartu"
projects:  ""
publishDate: 2016-01-28
selected: false
summary: ""
title: A neural network model for selectional preference in simplex clauses
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

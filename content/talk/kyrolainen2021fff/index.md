---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: 2021-11-04T13:00:00
date_end: 2021-11-04T13:00:00
draft: false
event: "The 62nd Annual Meeting of the Psychonomic Society"
event_url: ""
featured: false
location: "Virtual Conference"
projects:  ""
publishDate: 2021-11-04
selected: false
summary: ""
title: Narratives of personal life events by older adults before and during the pandemic - First findings from the CoSoWELL corpus
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

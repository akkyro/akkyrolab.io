---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2012-06-04T13:00:00
date_end: 2012-06-04T13:00:00
draft: false
event: "Non-Canonically Case-Marked Subjects within and across Languages and Language Families"
event_url: ""
featured: false
location: "Reykjavik"
projects:  ""
publishDate: 2012-06-04
selected: false
summary: ""
title: Being a dative subject - Patterns, contextual properties and usage
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

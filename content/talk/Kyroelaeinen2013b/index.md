---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2013-05-23T13:00:00
date_end: 2013-05-23T13:00:00
draft: false
event: "NA"
event_url: ""
featured: false
location: "University of Tartu"
projects:  ""
publishDate: 2013-05-23
selected: false
summary: ""
title: Lexical networks in action
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

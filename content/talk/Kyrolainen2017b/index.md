---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Seppo Vainio
- M. Juhani Luotolahti
- Filip Ginter
- Jukka Hyönä
date: 2017-07-10T13:00:00
date_end: 2017-07-10T13:00:00
draft: false
event: "The 14th International Cognitive Linguistics Conference"
event_url: ""
featured: false
location: "University of Tartu"
projects:  ""
publishDate: 2017-07-10
selected: false
summary: ""
title: Aspects of expectation - The role of expectations in processing grammatical aspect during reading
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

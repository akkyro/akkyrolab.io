---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Seppo Vainio
- Jukka Hyönä
date: 2018-06-10T13:00:00
date_end: 2018-06-10T13:00:00
draft: false
event: "The 1st International Workshop on Predictive Processing"
event_url: ""
featured: false
location: "San Sebasti'an"
projects:  ""
publishDate: 2018-06-10
selected: false
summary: ""
title: Unfolding events - The role of syntactic information in predictive processing during reading
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Daniil Gnetov
- Aki-Juhani Kyröläinen
date: 2018-10-01T13:00:00
date_end: 2018-10-01T13:00:00
draft: false
event: "Experimental Studies of Language and Speech: Bilingualism and Multilingualism (E-SoLaS)"
event_url: ""
featured: false
location: "Tomsk State University"
projects:  ""
publishDate: 2018-10-01
selected: false
summary: ""
title: The effect of morphological family size on response latency in a lexical decision task
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2011-03-24T13:00:00
date_end: 2011-03-24T13:00:00
draft: false
event: "Constructional and lexical semantic approaches to Russian"
event_url: ""
featured: false
location: "The Russian Academy of Sciences"
projects:  ""
publishDate: 2011-03-24
selected: false
summary: ""
title: Building conceptual spaces - Modeling the Russian reflexive constructions (-emphsja) with random forests and clustering
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

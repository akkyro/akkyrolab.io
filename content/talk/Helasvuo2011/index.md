---
abstract: ""
authors: 
- Marja-Liisa Helasvuo
- Aki-Juhani Kyröläinen
date: 2011-04-01T13:00:00
date_end: 2011-04-01T13:00:00
draft: false
event: "Käyttöpohjaisia näkökulmia kielioppiin (Usage-Based Approaches to Grammar)"
event_url: ""
featured: false
location: "University of Tartu"
projects:  ""
publishDate: 2011-04-01
selected: false
summary: ""
title: Ilmisubjekti vai nolla? Syntaktisen variaation kontekstuaaliset piirteet tarkastelussa
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

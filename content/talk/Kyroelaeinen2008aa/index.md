---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2008-10-17T13:00:00
date_end: 2008-10-17T13:00:00
draft: false
event: "Kieli keskellä kognitiota IV"
event_url: ""
featured: false
location: "FiCLAn laivaseminaari (seminar by the Finnish Cognitive Linguistics Association FiCLA)"
projects:  ""
publishDate: 2008-10-17
selected: false
summary: ""
title: Kategoriat, polysemia ja granulariteetti (Categories, polysemy and granularity)
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

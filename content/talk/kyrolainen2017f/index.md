---
abstract: ""
authors: 
- Veronika Laippala
- Aki-Juhani Kyröläinen
- Aleksi Vesanto
- Lotta Lehti
- Johanna Kallio
- Filip Ginter
date: 2017-10-18T13:00:00
date_end: 2017-10-18T13:00:00
draft: false
event: "HELDIG Digital Humanities Summit"
event_url: ""
featured: false
location: "University of Helsinki"
projects:  ""
publishDate: 2017-10-18
selected: false
summary: ""
title: Welfare bums or fellow humans - Discussing welfare provision to the poor in Suomi24 forum
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2019-05-10T13:00:00
date_end: 2019-05-10T13:00:00
draft: false
event: "Student Research Day"
event_url: ""
featured: false
location: "McMaster University"
projects:  ""
publishDate: 2019-05-10
selected: false
summary: ""
title: Language and lifelong learning
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

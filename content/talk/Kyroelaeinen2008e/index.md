---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2008-03-26T13:00:00
date_end: 2008-03-26T13:00:00
draft: false
event: "Corpus Methods in Linguistics and Language Pedagogy"
event_url: ""
featured: false
location: "University of Chicago"
projects:  ""
publishDate: 2008-03-26
selected: false
summary: ""
title: Russian reflexive marker - A layered model and the property domain
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

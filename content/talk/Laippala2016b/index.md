---
abstract: ""
authors: 
- Veronika Laippala
- Aki-Juhani Kyröläinen
date: 2016-01-28T13:00:00
date_end: 2016-01-28T13:00:00
draft: false
event: "IV soome-eesti grammatikakonverents (The 4th Finnish-Estonian Grammar Conference)"
event_url: ""
featured: false
location: "University of Tartu"
projects:  ""
publishDate: 2016-01-28
selected: false
summary: ""
title: Syntactic profiles of emoticons in the Finnish Internet
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

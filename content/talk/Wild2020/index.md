---
abstract: ""
authors: 
- Heather Wild
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: 2020-10-16T13:00:00
date_end: 2020-10-16T13:00:00
draft: false
event: "Words in the World (WoW) International Conference"
event_url: ""
featured: false
location: "Virtual Conference"
projects:  ""
publishDate: 2020-10-16
selected: false
summary: ""
title: Just how weird is WEIRD literacy and numeracy? A comparative study of 32 countries
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

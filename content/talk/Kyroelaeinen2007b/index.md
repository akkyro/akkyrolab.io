---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2007-05-24T13:00:00
date_end: 2007-05-24T13:00:00
draft: false
event: "Kielitieteen päivät (Language Days)"
event_url: ""
featured: false
location: "University of Oulu"
projects:  ""
publishDate: 2007-05-24
selected: false
summary: ""
title: Tapaus venäjän kielen refleksiiviverbi (About the Russian reflexive verbs)
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Marjut Johansson
- Aki-Juhani Kyröläinen
- Filip Ginter
- Attila Krizsán
- Veronika Laippala
date: 2016-05-25T13:00:00
date_end: 2016-05-25T13:00:00
draft: false
event: "Kielitieteen päivät (Language Days)"
event_url: ""
featured: false
location: "University of Oulu"
projects:  ""
publishDate: 2016-05-25
selected: false
summary: ""
title: "#je suis Charlie --Anatomy of a Twitter discussion with mixed methods"
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

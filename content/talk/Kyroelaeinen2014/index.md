---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2014-12-08T13:00:00
date_end: 2014-12-08T13:00:00
draft: false
event: "VISU kielioppisymposiumi (VISU Grammar Symposium)"
event_url: ""
featured: false
location: "University of Turku"
projects:  ""
publishDate: 2014-12-08
selected: false
summary: ""
title: The role of morphological verb constructions in processing grammatical aspect - Evidence from lexical decision and eye-tracking
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Lotta Lehti
- Johanna Isosävi
- Veronika Laippala
- Aki-Juhani Kyröläinen
- Filip Ginter
date: 2015-11-19T13:00:00
date_end: 2015-11-19T13:00:00
draft: false
event: "The 1st International Conference: Approaches to Digital Discourse Analysis"
event_url: ""
featured: false
location: "Valencia"
projects:  ""
publishDate: 2015-11-19
selected: false
summary: ""
title: Free speech doesn't mean careless talk! #IamnotCharlie - The development of multivoiced campaigns in Twitter following the January 2015 shootings in Paris
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Marja-Liisa Helasvuo
- Aki-Juhani Kyröläinen
date: 2010-12-10T13:00:00
date_end: 2010-12-10T13:00:00
draft: false
event: "Finnish Cognitive Linguistics Association FiCLA, Contexts of Language: How to Analyze Context?"
event_url: ""
featured: false
location: "University of Helsinki"
projects:  ""
publishDate: 2010-12-10
selected: false
summary: ""
title: A random forest model for contextually induced person marking strategies
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

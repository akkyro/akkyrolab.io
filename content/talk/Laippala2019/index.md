---
abstract: ""
authors: 
- Veronika Laippala
- Aki-Juhani Kyröläinen
- Filip Ginter
- Jesse Egbert
- Douglas Biber
date: 2019-07-22T13:00:00
date_end: 2019-07-22T13:00:00
draft: false
event: "The 10th International Corpus Linguistics Conference"
event_url: ""
featured: false
location: "Cardiff University"
projects:  ""
publishDate: 2019-07-22
selected: false
summary: ""
title: Generating online text types - A cluster analysis of predicted document embeddings
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

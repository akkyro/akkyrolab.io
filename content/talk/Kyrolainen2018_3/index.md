---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Filip Ginter
- Raymond Bertram
- Victor Kuperman
date: 2018-11-15T13:00:00
date_end: 2018-11-15T13:00:00
draft: false
event: "The 59th Annual Meeting of the Psychonomic Society"
event_url: ""
featured: false
location: "New Orleans, LA"
projects:  ""
publishDate: 2018-11-15
selected: false
summary: ""
title: Orthographic representation in flux - A large-scale analysis of spelling errors of Finnish nominal compounds
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

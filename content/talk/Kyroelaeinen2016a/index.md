---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- M. Juhani Luotolahti
- Filip Ginter
date: 2016-03-10T13:00:00
date_end: 2016-03-10T13:00:00
draft: false
event: "IV Finnish -- Estonian / I -- Hungarian Conference of Cognitive Linguistics"
event_url: ""
featured: false
location: "University of Turku"
projects:  ""
publishDate: 2016-03-10
selected: false
summary: ""
title: Smooth distributional semantic memory and selectional preference
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: 2020-12-07T13:00:00
date_end: 2020-12-07T13:00:00
draft: false
event: "MIRA & Labarge Research Showcase"
event_url: ""
featured: false
location: "Hamilton"
projects:  ""
publishDate: 2020-12-07
selected: false
summary: ""
title: Predicting loneliness
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2008-06-02T13:00:00
date_end: 2008-06-02T13:00:00
draft: false
event: "Workshop in Cognitive Grammar"
event_url: ""
featured: false
location: "University of Tartu"
projects:  ""
publishDate: 2008-06-02
selected: false
summary: ""
title: Russian reflexive marker and the property domain
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

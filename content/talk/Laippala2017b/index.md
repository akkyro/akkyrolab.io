---
abstract: ""
authors: 
- Veronika Laippala
- Aki-Juhani Kyröläinen
- Jenna Kanerva
- M. Juhani Luotolahti
- Tapio Salakoski
- Filip Ginter
date: 2017-03-14T13:00:00
date_end: 2017-03-14T13:00:00
draft: false
event: "The 2nd Conference of the association of Digital Humanities in the Nordic Countries (DHN)"
event_url: ""
featured: false
location: "University of Gothenburg"
projects:  ""
publishDate: 2017-03-14
selected: false
summary: ""
title: Finnish Internet Parsebank- A web-crawled corpus of Finnish with syntactic analyses
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

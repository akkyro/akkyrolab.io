---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2015-01-06T13:00:00
date_end: 2015-01-06T13:00:00
draft: false
event: "NA"
event_url: ""
featured: false
location: "Centre for Comparative Psycholinguistics, University of Alberta"
projects:  ""
publishDate: 2015-01-06
selected: false
summary: ""
title: Grammatical aspect
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

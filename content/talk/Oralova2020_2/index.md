---
abstract: ""
authors: 
- Gaisha Oralova
- Rober Boshra
- Daniel Schmidtke
- Aki-Juhani Kyröläinen
- John F. Connolly
- Victor Kuperman
date: 2020-11-19T13:00:00
date_end: 2020-11-19T13:00:00
draft: false
event: "The 61st Annual Meeting of the Psychonomic Society"
event_url: ""
featured: false
location: "Virtual Conference"
projects:  ""
publishDate: 2020-11-19
selected: false
summary: ""
title: Individual differences in spelling proficiency as reflected by simultaneous EEG and eye-tracking
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

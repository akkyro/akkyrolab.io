---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- James Gillet
- Ranil Sonnadara
- Victor Kuperman
date: 2019-12-12T13:00:00
date_end: 2019-12-12T13:00:00
draft: false
event: "MIRA & Labarge Research Day"
event_url: ""
featured: false
location: "McMaster Innovation Park"
projects:  ""
publishDate: 2019-12-12
selected: false
summary: ""
title: Reflections of loneliness in written life stories among older adults
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

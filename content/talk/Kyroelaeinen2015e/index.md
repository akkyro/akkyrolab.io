---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2015-08-27T13:00:00
date_end: 2015-08-27T13:00:00
draft: false
event: "NA"
event_url: ""
featured: false
location: "Quantitative Approaches to the Russian Language"
projects:  ""
publishDate: 2015-08-27
selected: false
summary: ""
title: Semantics or rules? Online processing of perfectivizing prefixes in Russian
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Veronika Laippala
- Aki-Juhani Kyröläinen
- M. Juhani Luotolahti
date: 2015-07-26T13:00:00
date_end: 2015-07-26T13:00:00
draft: false
event: "The 14th International Pragmatics Conference"
event_url: ""
featured: false
location: "Antwerp"
projects:  ""
publishDate: 2015-07-26
selected: false
summary: ""
title: Emoticons as indices of adaptability in the Finnish Internet
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

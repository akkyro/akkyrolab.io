---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2010-09-03T13:00:00
date_end: 2010-09-03T13:00:00
draft: false
event: "ICCG 6"
event_url: ""
featured: false
location: "Prague"
projects:  ""
publishDate: 2010-09-03
selected: false
summary: ""
title: From canon and monolith to clusters - A constructionist model of subjecthood
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

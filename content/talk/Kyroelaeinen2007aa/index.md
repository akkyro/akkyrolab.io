---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2007-08-23T13:00:00
date_end: 2007-08-23T13:00:00
draft: false
event: "'Aineistojen pauloissa'"
event_url: ""
featured: false
location: "University of Turku"
projects:  ""
publishDate: 2007-08-23
selected: false
summary: ""
title: Korpuslingvistiikasta ja korpuslingvistisistä metodeista (About corpus-linguistics and methods)
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

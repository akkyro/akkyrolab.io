---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: 2022-11-17T13:00:00
date_end: 2022-11-17T13:00:00
draft: false
event: "63rd Annual Meeting of the Psychonomic Society"
event_url: ""
featured: false
location: "Boston, MA"
projects:  ""
publishDate: 2022-11-17
selected: false
summary: ""
title: Social mobility profiles in older adulthood - Evidence from the Cognitive and Social Well-Being (CoSoWELL) project
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

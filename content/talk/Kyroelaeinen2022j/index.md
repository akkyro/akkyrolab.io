---
abstract: ""
authors: 
- Valtteri Skantsi
- Aki-Juhani Kyröläinen
- Laippala Veronika
date: 2022-12-01T13:00:00
date_end: 2022-12-01T13:00:00
draft: false
event: "NA"
event_url: ""
featured: false
location: "Jyväskylä"
projects:  ""
publishDate: 2022-12-01
selected: false
summary: ""
title: The Contribution of Topics to Register Variation in Web Documents
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

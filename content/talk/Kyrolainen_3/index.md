---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- M. Juhani Luotolahti
- Kai Hakala
- Filip Ginter
date: 2016-08-15T13:00:00
date_end: 2016-08-15T13:00:00
draft: false
event: "DSALT: Distributional semantics and linguistic theory"
event_url: ""
featured: false
location: "Bolzano"
projects:  ""
publishDate: 2016-08-15
selected: false
summary: ""
title: Modeling cloze probabilities and selectional preferences with neural networks
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

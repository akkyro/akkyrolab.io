---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Antti Leino
date: 2010-05-20T13:00:00
date_end: 2010-05-20T13:00:00
draft: false
event: "Kielitieteen päivät (Language days)"
event_url: ""
featured: false
location: "University of Helsinki"
projects:  ""
publishDate: 2010-05-20
selected: false
summary: ""
title: Voiko subjektiivisuutta mitata? (Can you measure subjectivity?)
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

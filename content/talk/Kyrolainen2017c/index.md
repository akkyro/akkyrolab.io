---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Seppo Vainio
- Jukka Hyönä
date: 2017-06-22T13:00:00
date_end: 2017-06-22T13:00:00
draft: false
event: "International Morphological Processing Conference (MoProc)"
event_url: ""
featured: false
location: "Scuola Internazionale Superiore di Studi Avanzati (SISSA)"
projects:  ""
publishDate: 2017-06-22
selected: false
summary: ""
title: The role of surprisal in processing grammatical aspect during reading
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

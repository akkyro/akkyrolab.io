---
abstract: ""
authors: 
- Marja-Liisa Helasvuo
- Aki-Juhani Kyröläinen
date: 2011-03-21T13:00:00
date_end: 2011-03-21T13:00:00
draft: false
event: "Utuling"
event_url: ""
featured: false
location: "University of Turku"
projects:  ""
publishDate: 2011-03-21
selected: false
summary: ""
title: Ilmisubjekti vai nolla? Syntaktisen variaation kontekstuaaliset piirteet tarkastelussa (Zero or overt? Syntactic variation through contextual features)
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Marja-Liisa Helasvuo
- Aki-Juhani Kyröläinen
date: 2013-12-08T13:00:00
date_end: 2013-12-08T13:00:00
draft: false
event: "VISU kielioppisymposiumi (VISU Grammar Symposium)"
event_url: ""
featured: false
location: "University of Turku"
projects:  ""
publishDate: 2013-12-08
selected: false
summary: ""
title: To choose a zero - Modeling subject expression in the 3rd person singular in Finnish conversation
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

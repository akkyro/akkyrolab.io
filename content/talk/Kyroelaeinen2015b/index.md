---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Vincent Porretta
- Juhani Järvikivi
date: 2015-09-28T13:00:00
date_end: 2015-09-28T13:00:00
draft: false
event: "New Developments in the Quantitative Study of Languages, Symposium of the Linguistics Association of Finland (SKY)"
event_url: ""
featured: false
location: "University of Helsinki"
projects:  ""
publishDate: 2015-09-28
selected: false
summary: ""
title: Pupillometry as a window to real time processing of morphologically complex verbs
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

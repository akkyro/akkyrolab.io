---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2008-10-01T13:00:00
date_end: 2008-10-01T13:00:00
draft: false
event: "Langnetin kielen elinkaari -seminaari (Langnet Life Span of Language Seminar)"
event_url: ""
featured: false
location: "Athens"
projects:  ""
publishDate: 2008-10-01
selected: false
summary: ""
title: From variation to system
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

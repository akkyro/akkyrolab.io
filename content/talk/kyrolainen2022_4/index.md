---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Samuel Rönnqvist
- Sampo Pyysalo
- Filip Ginter
- Veronika Laippala
date: 2022-09-09T13:00:00
date_end: 2022-09-09T13:00:00
draft: false
event: "The 15th International American Association for Corpus Linguistics Conference"
event_url: ""
featured: false
location: "Northern Arizona University"
projects:  ""
publishDate: 2022-09-09
selected: false
summary: ""
title: Towards universal tendencies in register variation
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

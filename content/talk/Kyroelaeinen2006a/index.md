---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2006-12-11T13:00:00
date_end: 2006-12-11T13:00:00
draft: false
event: "Suomalais-virolainen kognitiivisen kielitieteen konferenssi (Finnish-Estonian Cognitive Linguistics Conference)"
event_url: ""
featured: false
location: "Taagepera"
projects:  ""
publishDate: 2006-12-11
selected: false
summary: ""
title: Subjektiivisen olotilan ilmaiseminen venäjän kielessä (Expressing the subjective state in Russian)
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

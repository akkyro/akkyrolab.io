---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2016-05-24T13:00:00
date_end: 2016-05-24T13:00:00
draft: false
event: "Construal of Means and Time (COMET) seminar"
event_url: ""
featured: false
location: "University of Oulu"
projects:  ""
publishDate: 2016-05-24
selected: false
summary: ""
title: Semanttinen samankaltaisuus ja yllätyksellisyys aspektin prosessoinnissa (The role of semantic similarity and surprisal in processing aspect)
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

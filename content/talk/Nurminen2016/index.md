---
abstract: ""
authors: 
- Salla Nurminen
- Aki-Juhani Kyröläinen
date: 2016-05-25T13:00:00
date_end: 2016-05-25T13:00:00
draft: false
event: "Kielitieteen päivät (Language days)"
event_url: ""
featured: false
location: "University of Oulu"
projects:  ""
publishDate: 2016-05-25
selected: false
summary: ""
title: Leksikaalinen aspekti suomen kielessä (Lexical aspect in Finnish)
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2007-10-12T13:00:00
date_end: 2007-10-12T13:00:00
draft: false
event: "Kieli keskellä kognitiota III"
event_url: ""
featured: false
location: "FiCLAn laivaseminaari (seminar by the Finnish Cognitive Linguistics Association FiCLA)"
projects:  ""
publishDate: 2007-10-12
selected: false
summary: ""
title: Matalan frekvenssin konstruktiot ja salienttisuus (Low frequency constructions and saliency)
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

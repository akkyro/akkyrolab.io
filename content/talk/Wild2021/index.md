---
abstract: ""
authors: 
- Heather Wild
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: 2021-07-13T13:00:00
date_end: 2021-07-13T13:00:00
draft: false
event: "Society for the Scientific Study of Reading Conference"
event_url: ""
featured: false
location: "Virtual Meeting"
projects:  ""
publishDate: 2021-07-13
selected: false
summary: ""
title: Using canonical correlation to explore differences in the distribution of literacy and numeracy skills
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

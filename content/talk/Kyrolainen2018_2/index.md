---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Victor Kuperman
date: 2018-09-25T13:00:00
date_end: 2018-09-25T13:00:00
draft: false
event: "The 11th International Conference of the Mental Lexicon"
event_url: ""
featured: false
location: "University of Alberta"
projects:  ""
publishDate: 2018-09-25
selected: false
summary: ""
title: Forty years of experience and counting
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

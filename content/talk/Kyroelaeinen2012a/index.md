---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Kristel Uiboaed
date: 2012-08-05T13:00:00
date_end: 2012-08-05T13:00:00
draft: false
event: "AVML"
event_url: ""
featured: false
location: "York, UK"
projects:  ""
publishDate: 2012-08-05
selected: false
summary: ""
title: Dialectal data visualization and statistical manipulation with ggmap-package in emphR
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
date: 2011-09-28T13:00:00
date_end: 2011-09-28T13:00:00
draft: false
event: "NA"
event_url: ""
featured: false
location: "University of Tartu"
projects:  ""
publishDate: 2011-09-28
selected: false
summary: ""
title: Exploring structure with random forests and clustering - A constructionist model of the Russian reflexive marker (-emphsja)
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Mirjam Ruutma
- Aki-Juhani Kyröläinen
- Maarja-Liisa Pilvik
- Kristel Uiboaed
date: 2014-12-08T13:00:00
date_end: 2014-12-08T13:00:00
draft: false
event: "VISU kielioppisymposiumi (VISU Grammar Symposium)"
event_url: ""
featured: false
location: "University of Turku"
projects:  ""
publishDate: 2014-12-08
selected: false
summary: ""
title: Puudest läbi ja üle piirkonna - ambipositsioonide süntaktiline varieerumine eesti murretes
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

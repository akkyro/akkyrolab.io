---
abstract: ""
authors: 
- Victor Kuperman
- Aki-Juhani Kyröläinen
date: 2021-11-04T13:00:00
date_end: 2021-11-04T13:00:00
draft: false
event: "The 62nd Annual Meeting of the Psychonomic Society"
event_url: ""
featured: false
location: "Virtual Conference"
projects:  ""
publishDate: 2021-11-04
selected: false
summary: ""
title: Temporal dynamics of emotional well-being and loneliness in older adults during the first year of the COVID-19 pandemic - Insights from the Cognitive and Social Well-Being (CoSoWELL) corpus
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

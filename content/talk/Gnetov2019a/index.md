---
abstract: ""
authors: 
- Daniil Gnetov
- Aki-Juhani Kyröläinen
date: 2019-09-06T13:00:00
date_end: 2019-09-06T13:00:00
draft: false
event: "The 25th Architectures and Mechanisms of Language Processing Conference"
event_url: ""
featured: false
location: "Moscow"
projects:  ""
publishDate: 2019-09-06
selected: false
summary: ""
title: Quantifications of morphological family size - Combining word embeddings with lexical decision in Russian
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Veronika Laippala
- Aki-Juhani Kyröläinen
- Filip Ginter
- Jenna Kanerva
- Johanna Komppa
- Jyrki Kalliokoski
date: 2018-03-19T13:00:00
date_end: 2018-03-19T13:00:00
draft: false
event: "TextLink2018 -- Final Action Conference. Cross-Linguistic Discourse Annotation: applications and perspectives"
event_url: ""
featured: false
location: "Toulouse"
projects:  ""
publishDate: 2018-03-19
selected: false
summary: ""
title: A bottom-up analysis of sentence-initial DRDs in the Finnish Internet
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

---
abstract: ""
authors: 
- Aki-Juhani Kyröläinen
- Kristina Geeraert
date: 2013-09-12T13:00:00
date_end: 2013-09-12T13:00:00
draft: false
event: "The 5th Conference on Quantitative Investigations in Theoretical Linguistics"
event_url: ""
featured: false
location: "The Katholieke Universiteit Leuven"
projects:  ""
publishDate: 2013-09-12
selected: false
summary: ""
title: The Relationship between form and meaning - Modelling semantic densities of English monomorphemic verbs
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

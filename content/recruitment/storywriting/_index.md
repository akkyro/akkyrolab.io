---
date: "2018-09-09T00:00:00Z"
draft: false
lastmod: "2018-09-09T00:00:00Z"
linktitle: Online life story writing
menu:
#  example:
#    name: Overview
#    weight: 1
summary: We are inviting older adults to take part in our online story writing task.
title: Story writing
toc: true
type:
weight: 1
---

## Description

Are you an older adult 55+ years of age interested in taking part in an online writing study? If so, McMaster University is offering participation in a simple, anonymous online experiment that provides you with the opportunity to reflect on and share your life experiences right from the comfort of your own home.

* **Name**: Linguistic markers of well-being in adulthood
* **Purpose**: To better understand cognitive well-being in older adulthood
* **Location**: [Online link](https://surveys.mcmaster.ca/limesurvey/index.php/387357?lang=en)
* **Activities**: Complete a background questionnaire, write four stories, and complete a social isolation questionnaire
* **Length**: Approximately 25-45 minutes
* **Language of Activity**: English
* **Principal Investigator**: Dr. Victor Kuperman

* **PARTICIPATE**: To participate, navigate to the website provided in the “Location” field above.

If you have any questions about this study, please contact Dr. Victor Kuperman at lfstory@mcmaster.ca.

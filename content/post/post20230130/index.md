---
authors:
- admin
categories:
- Publications
date: "2023-01-30T00:00:00Z"
draft: false
featured: false
image:
  # caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
  # focal_point: ""
  # placement: 2
  # preview_only: false
lastmod: "2023-01-30T00:00:00Z"
projects:
- CoSoWell
subtitle:
summary:
tags:
- aging
title: 'CoSoWELL version 1 published'
---

The site has been updated! The first version of Cognitive and Social Well-Being (CoSoWELL) corpus has been made publicly available!!

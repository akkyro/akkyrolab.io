---
authors:
- admin
categories: []
date: "2021-10-17T00:00:00Z"
draft: false
featured: false
image:
  # caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
  # focal_point: ""
  # placement: 2
  # preview_only: false
lastmod: "2021-10-17T00:00:00Z"
projects:
- CoSoWell
subtitle:
summary: 
tags:
title: 'Upcoming presentation at Psychonomics 2021!'
---

Excellent news related to my aging projects. I have two presentations at the upcoming 60th annual meeting of Psychonomic Society in November. Both talks are related to my current project on cognitive and social well-being in Adulhood (CoSoWELL).

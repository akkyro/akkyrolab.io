---
authors:
- admin
categories:
- Publications
date: "2020-07-16T00:00:00Z"
draft: false
featured: false
image:
  # caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
  # focal_point: ""
  # placement: 2
  # preview_only: false
lastmod: "2020-07-16T00:00:00Z"
projects: 
- LexAffect
subtitle: 'Affect across adulthood'
summary: 
tags:
- affect
- aging
- emotion
title: 'New publication'
---

Our article, *Affect across adulthood: Evidence from English, Dutch and Spanish*, has been accepted for publication in Journal of Experimental Psychology: General. 
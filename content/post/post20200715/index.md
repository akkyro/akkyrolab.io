---
authors:
- admin
categories: []
date: "2020-07-15T00:00:00Z"
draft: false
featured: false
image:
  # caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
  # focal_point: ""
  # placement: 2
  # preview_only: false
lastmod: "2020-07-15T00:00:00Z"
projects: 
- CoSoWell
subtitle: 'WritLarge'
summary: 
tags:
- text analytics
- aging
title: 'App Launch'
---

Our app, [WritLarge](https://akkyro.shinyapps.io/writlarge/), has officially launched, as part of the CoSoWell project. The app provides users a convenient interface for reading life stories written by older adults. 
---
authors:
- admin
categories:
- Publications
date: "2022-04-10T00:00:00Z"
draft: false
featured: false
image:
  # caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
  # focal_point: ""
  # placement: 2
  # preview_only: false
lastmod: "2022-04-10T00:00:00Z"
projects:
- LexAffect
subtitle: 'Effects of age and the pandemic'
summary:
tags:
- affect
- aging
- emotion
title: 'Our new valency norms have been published'
---

Excellent news related to my aging projects. New valency norms for English are now publicly available covering 3,600 words. The norms were collected during COVID-19 from younger and older adults!

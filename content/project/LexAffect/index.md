---
date: "2020-07-16T00:00:00Z"
external_link: ""
image:
  caption: 
  focal_point: 
links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
slides: 
summary: LexAffect project page
tags:
- aging
- emotion
- text analytics
- unsupervised learning
title: LexAffect Project
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

Emotions play a fundamental role in language learning, use and processing. Words denoting positivity account for a larger part of the lexicon than words denoting negativity, and they also tend to be used more frequently, a phenomenon known as positivity bias.
The LexAffect project examines the impact of aging on the affective lexicon in a series of publications entitled:

* [Affect across adulthood: Evidence from English, Dutch, and Spanish](https://doi.apa.org/doiLanding?doi=10.1037%2Fxge0000950)
* [Valence norms for 3,600 English words collected during the COVID-19 pandemic: Effects of age and](https://link.springer.com/article/10.3758/s13428-021-01740-0)
* The pandemic and Temporal dynamics of emotional well-being and loneliness in older adults during the first year of the COVID-19 pandemic: Insights from the Cognitive and Social Well-Being (CoSoWELL) corpus. (Currently under review)

As part of this project, we have also released a new set of valency norms for 3,600 English words collected during the COVID-19 pandemic from younger and older adults. The data are publicly available at [OSF](https://osf.io/e6px8/?view_only=0bef4b18a10e4397b3dd04d7fb60559b).

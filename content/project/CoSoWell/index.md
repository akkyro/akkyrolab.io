---
date: "2020-07-16T00:00:00Z"
external_link: ""
image:
  caption: 
  focal_point: Smart
links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
slides: 
summary: CoSoWell project page
tags:
- aging
- loneliness
- text analytics
title: CoSoWell Project
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

Cognitive and Social Well-being (CoSoWell) is a project that connects linguistics research and cognitive aging studies to machine learning.
Aging brings forth changes in life circumstances, some of which can negatively affect the quality of life and well-being of older adults such as social isolation and loneliness. In this project we focus on the interplay between language use and social mobility.
The first version of CoSoWELL (version 1) has been released. The data are publicaly available and have a dedicated repository at [OSF](https://osf.io/x4s28/?view_only=659fc5216ca1477e801306d05ac0feaa). The theoretical basis of forming the corpus and its description along with the validation of the collected written life stories can be found in our article entitled [Cognitive and social well-being in older adulthood: The CoSoWELL corpus of written life stories](https://link.springer.com/article/10.3758/s13428-022-01926-0). The collected life stories are also discussed from a qualitative perspective in our recent publications entitled [Personhood and aging: Exploring the written narratives of older adults as articulations of personhood in later life](https://doi.org/10.1016/j.jaging.2022.101040).

The first set of stories have also been made available to the public via a web-based Shiny app [WritLarge](https://akkyro.shinyapps.io/writlarge/).

As part of this project, we are committed to promoting social mobility and social engagement among older adults. As part of this commitment, we are using machine learning to identify mappings between language use as reflected in written life stories and social mobility.


---
date: "2021-10-17"
external_link: ""
image:
  caption: 
  focal_point: 
links:
slides: 
summary: "PupilPre: Preprocessing Pupil Size Data"
tags:
- R
- eye-tracking
- pupil dilation
- shiny
- R Markdown
title: PupilPre
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

I developed and maintain *PupilPre*, an R package for preprocessing and plotting pupil size data. This work is done together with <a href="https://vincentporretta.gitlab.io/" target="_blank">Vincent Porretta</a>, <a href="http://www.jacolienvanrij.com/" target="_blank">Jacolien van Rij</a> and <a href="https://sites.ualberta.ca/~jarvikiv/" target="_blank">Juhani Järvikivi</a>. The package is built in conjunction with and relies upon *VWPre*. The current version of the package can be found on CRAN:
<a href="https://CRAN.R-project.org/package=PupilPre" target="_blank">PupilPre</a> 

As with *VWPre*, there are several elaborated vignettes designed to help users become comfortable with the functionality of the package. 

<img src="http://cranlogs.r-pkg.org/badges/PupilPre" align="bottom" height="25" width="150"> 

<br><br>

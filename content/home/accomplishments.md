+++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 70  # Order that this section will appear.

title = "Funding"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  organization = "MIRA/LCMA"
  organization_url = "https://mira.mcmaster.ca/"
  title = "Relieving social isolation and loneliness through storytelling at the time of a pandemic"
  url = ""
  certificate_url = ""
  date_start = "2020-09-01"
  date_end = "2021-08-31"
  description = "Labarge Centre for Mobility in Aging COVID-19 Grant (Co-PI)"
  
[[item]]
  organization = "MIRA/AGE-WELL"
  organization_url = "https://agewell-nce.ca"
  title = "Aging and narratives: Linguistic markers of social well-being in late adulthood"
  url = ""
  certificate_url = ""
  date_start = "2020-04-30"
  date_end = "2021-03-31"
  description = "Renewal of AGE-WELL Graduate Student and Postdoctoral Award in Technology and Aging (Personal Award)"

[[item]]
  organization = "SSHRC"
  organization_url = "https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx"
  title = "Speaking of age: Linguistic markers of cognitive, emotional and social wellbeing in late adulthood"
  url = ""
  certificate_url = ""
  date_start = "2019-06-01"
  date_end = "2021-05-31"
  description = "SSHRC: Insight Development Grant (PI)"

[[item]]
  organization = "MIRA/LCMA"
  organization_url = "https://mira.mcmaster.ca/"
  title = "Writing of age: Linguistic markers of cognitive, emotional and social well-being among older adults"
  url = ""
  certificate_url = ""
  date_start = "2019-08-01"
  date_end = "2020-07-31"
  description = "Catalyst Grant (collaborator)"
  
[[item]]
  organization = "MIRA/AGE-WELL"
  organization_url = "https://agewell-nce.ca"
  title = "Aging and narratives: Linguistic markers of social well-being in late adulthood"
  url = ""
  certificate_url = ""
  date_start = "2019-10-01"
  date_end = "2020-03-31"
  description = "AGE-WELL Graduate Student and Postdoctoral Award in Technology and Aging (Personal Award)"

+++

+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Researcher"
  company = "University of Turku"
  company_url = ""
  location = "Finland"
  date_start = "2021-06-01"
  date_end = ""
  description = """"""

[[experience]]
  title = "Postdoctoral Research Fellow"
  company = "McMaster University / Brock University"
  company_url = ""
  location = "Canada"
  date_start = "2017-10-01"
  date_end = "2021-05-30"
  description = """"""

[[experience]]
  title = "Postdoctoral Research Fellow"
  company = "University of Turku"
  company_url = ""
  location = "Finland"
  date_start = "2015-05-01"
  date_end = "2017-09-30"
  description = """"""

[[experience]]
  title = "Visiting Scholar"
  company = "University of Tartu"
  company_url = ""
  location = "Estonia"
  date_start = "2015-01-01"
  date_end = "2015-04-30"
  description = """"""

[[experience]]
  title = "Postdoctoral Research Fellow"
  company = "University of Turku"
  company_url = ""
  location = "Estonia"
  date_start = "2014-09-01"
  date_end = "2014-12-31"
  description = """"""

[[experience]]
  title = "Visiting professor"
  company = "University of Tartu"
  company_url = ""
  location = "Estonia"
  date_start = "2013-01-01"
  date_end = "2014-12-31"
  description = """"""

+++

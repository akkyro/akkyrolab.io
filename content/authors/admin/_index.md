---
authors:
- admin
bio: My research interests include...
education:
  courses:
  - course: PhD in Russian Linguistics
    institution: University of Turku
    year: 2013
  - course: MA in Russian Studies
    institution: University of Turku
    year: 2006
email: ""
interests:
- Aging
- Language processing
- Language production
- Natural language processing (NLP)
- Corpus linguistics
- Machine learning
organizations:
- name: University of Turku
  url: "https://www.utu.fi/en"

role: Researcher
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: researchgate
  icon_pack: ai
  link: https://www.researchgate.net/profile/Aki_Juhani_Kyroelaeinen
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.fi/citations?hl=en&user=nJ7FleIAAAAJ&view_op=list_works&gmla=AJsN-F66mvARIQk2wFpOsM5uEI8Tyv5zjVQ6QbBsbfGFqn58Eq3ERxZk3CDJXBpxStDYPyqmIE8SLzxYtavfPl9GjBC254x--W4hMSFIFKIbarPUe4tZh5M
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/aki-juhani-kyröläinen-language-researcher
superuser: true
title: Aki-Juhani Kyröläinen
user_groups:
- Researchers
- Visitors
---

I am a computational linguist who additionally employs psycholinguistic methods. Currently, I am a researcher at the University of Turku. As part of the TurkuNLP group, my research focuses on explainability of language models. My psycholinguistic research program is broadly centered on investigating the role of healthy aging on language comprehension and production which I started as a postdoctoral research fellow at McMaster University.
